import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Cart } from "../entities/cart.entity";
import { CartItem } from "../entities/cart_item.entity";


@Injectable()
export class CartService {
    constructor(
        @InjectRepository(Cart)
        private CartRepository: Repository<Cart>,
        // @InjectRepository(CartItem)
        // private CartItemRepository: Repository<CartItem>
    ){}

    findAll(): Promise<Cart[]> {
        return this.CartRepository.find({ relations: ["cartItem"] });
    }

    findOne(id: string): Promise<Cart> {
        return this.CartRepository.findOne(id);
    }

    createCart(cart: Cart): Promise<Cart> {
        
        return this.CartRepository.save(cart);
    }
}