import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class Product {

    @PrimaryGeneratedColumn()
    product_id: number;

    @Column()
    shop_id: number;

    @Column()
    product_name: string;

    @Column()
    product_price: number;


    @Column()
    product_desc: string;

    @Column()
    product_qty: string;




}
