import { Column, Entity, PrimaryGeneratedColumn ,OneToMany,JoinColumn } from "typeorm";
import {CartItem} from "./cart_item.entity";

@Entity()
export class Cart {
    @PrimaryGeneratedColumn()
    cart_id: number;

    @Column()
    shop_id: number;

    @Column()
    status: number;

    @Column()
    user_id: number;

       
    @Column() 
    createdAt: Date;

    @Column() 
    updatedAt: Date;


    @OneToMany(() => CartItem, CartItem => CartItem.cart)
    @JoinColumn({name: "cart_id"})

    cartItem: CartItem[];

  

}

