import { Column, Entity, PrimaryGeneratedColumn,ManyToOne,JoinColumn} from "typeorm";
import {Cart} from "./cart.entity";

@Entity()
export class CartItem {
    @PrimaryGeneratedColumn()
    cart_item_id: number;

    @Column()
    cart_id: number;

    @Column()
    product_id: number;

    @Column()
    product_qty: number;

    @Column()
    product_price: number;


    // @Column()
    // product_price: Double;

    
    @Column() 
    createdAt: Date;

    @Column() 
    updatedAt: Date;


    @ManyToOne(() => Cart, cart => cart.cartItem, { cascade: ['insert', 'update'] })
    @JoinColumn({name: "cart_id"})
    cart: Cart;



}