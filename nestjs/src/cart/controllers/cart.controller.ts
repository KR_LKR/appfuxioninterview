import { Body, Controller, Get, HttpStatus, Param, Post, Res } from "@nestjs/common";
import { Cart } from "../entities/cart.entity";
import { CartItem } from "../entities/cart_item.entity";
import { CartService } from "../services/cart.service";
import { Product } from "../entities/product.entity";
import { Double, Repository } from "typeorm";
import { Connection,getConnection} from 'typeorm';
import { IsEmail, isNotEmpty, IsNotEmpty } from 'class-validator';

export class cartItem {
  @IsNotEmpty()
  user_id: number;
}

export class addItem {
    @IsNotEmpty()
    product_id: number
    @IsNotEmpty()
    user_id: number;
    @IsNotEmpty()
    shop_id: number;
  }

  export class removeItem {
    @IsNotEmpty()
    cart_item_id: number
  }


@Controller('cart')
export class CartController {
    constructor(private readonly CartService: CartService,private connection: Connection){
    }
     con=this.connection.manager


    //     @Post()
    // async createCart(@Res() response, @Body()cart: Cart) {
    //     console.log(cart)
    //     const newBook = await this.CartService.createCart(cart);
    //     return response.status(HttpStatus.CREATED).json({
    //         newBook
    //     })
        
    // }

    @Get('test')
    async test(@Res() response) {
 
        const cartRepo = this.connection.getRepository(Cart);
        
        const cart = new Cart();

        const cartitem = new CartItem();
        cartitem.product_id = 1;
        cartitem.product_qty = 1;
        await this.con.save(cartitem);



        cart.shop_id = 1;
        cart.status=1;
        cart.user_id=1;
        cart.cartItem=[cartitem]
        this.con.save(cart)
        return response.status(HttpStatus.OK).json({
            cart
        })

    }



    @Get('cartList')
  async  cartList(@Res() response,@Body() cartItem: cartItem) {
        const cartRepo = this.connection.getRepository(Cart);

       let cart = await this.connection.getRepository(Cart)
        .createQueryBuilder("cart")
        .leftJoinAndSelect("cart.cartItem", "cartItem")
        .andWhere("cart.user_id = :user_id", { user_id: cartItem.user_id })
        .andWhere("cartItem.product_qty >=1 ")
        .getMany();
        
        return response.status(HttpStatus.OK).json({
            cart
        })
    }


    @Post('addProduct')
  async  addProduct(@Res() response,@Body() addItem: addItem) {
    let product = await this.connection.getRepository(Product)
    .createQueryBuilder("product")
    .where("product.product_id = :product_id", { product_id: addItem.product_id }).getOne()
    console.log(product)
    if(!product)
    {
        return response.status(HttpStatus.BAD_REQUEST).json({
            message:"product not found"
        })
    }
    else{
        this.connection.getRepository(Cart)
        .createQueryBuilder("cart")
        .leftJoinAndSelect("cart.cartItem", "cartItem")
        .where("cartItem.product_id = :product_id", { product_id: addItem.product_id })
        .andWhere("cart.user_id = :user_id", { user_id: addItem.user_id })
        .andWhere("cartItem.product_qty >=1 ")
        .andWhere("cartItem.product_price =:product_price",{product_price:product.product_price})
        .getOne().then(async data=>{
            if(data){
                let item=data.cartItem[0]
                     getConnection()
                    .createQueryBuilder()
                    .update(CartItem)
                    .set({ 
                        product_qty: item.product_qty+1
                    })
                    .where("cart_item_id = :cart_item_id", { cart_item_id: item.cart_item_id })
                    .execute();
            }
            else{
                const cart = new Cart();
                const cartitem = new CartItem();
                cartitem.product_id = addItem.product_id;
                cartitem.product_qty = 1;
                cartitem.product_price=product.product_price
                await this.con.save(cartitem);
                cart.shop_id = product.shop_id;
                cart.user_id=addItem.user_id;
                cart.cartItem=[cartitem]
                this.con.save(cart)

                }
            return response.status(HttpStatus.OK).json({
                data
            })
        })
    }       
    }
    @Post('deductProduct')
    async  deductProduct(@Res() response,@Body() removeItem: removeItem) {
    
          this.connection.getRepository(CartItem)
          .createQueryBuilder("cart")
          .where("cart.cart_item_id = :cart_item_id", { cart_item_id: removeItem.cart_item_id }).getOne().then(data=>{
              if(data){
                  let item=data.product_qty
                       getConnection()
                      .createQueryBuilder()
                      .update(CartItem)
                      .set({ 
                          product_qty: data.product_qty-1,
                          updatedAt:Date()
                      })
                      .where("cart_item_id = :cart_item_id", { cart_item_id: removeItem.cart_item_id })
                      .execute();
              }
              else{
                return response.status(HttpStatus.BAD_REQUEST).json({
                    message:"product not found"
                })
                  }
              return response.status(HttpStatus.OK).json({
                  data
              })
  
          })
      }

    @Get()
    async fetchAll(@Res() response) {
        const cart = await this.CartService.findAll();
        return response.status(HttpStatus.OK).json({
            cart
        })
    }

    @Get('/:id')
    async findById(@Res() response, @Param('id') id) {
        const book = await this.CartService.findOne(id);
        return response.status(HttpStatus.OK).json({
            book
        })
    }
}