import { Body, Controller, Get, HttpStatus, Param, Post, Res } from "@nestjs/common";
import { Cart } from "../entities/cart.entity";
import { CartService } from "../services/cart.service";

@Controller('cartItem')
export class CartController {
    constructor(private readonly CartService: CartService){}

    @Post()
    async createCart(@Res() response, @Body()cart: Cart) {
        const newBook = await this.CartService.createCart(cart);
        return response.status(HttpStatus.CREATED).json({
            newBook
        })
    }

    @Get()
    async fetchAll(@Res() response) {
        const books = await this.CartService.findAll();
        return response.status(HttpStatus.OK).json({
            books
        })
    }

    @Get('/:id')
    async findById(@Res() response, @Param('id') id) {
        const book = await this.CartService.findOne(id);
        return response.status(HttpStatus.OK).json({
            book
        })
    }

}