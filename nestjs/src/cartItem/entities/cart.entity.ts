import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Cart {
    @PrimaryGeneratedColumn()
    cart_id: number;

    @Column()
    shop_id: string;

    @Column()
    status: string;

    @Column()
    user_id: number;

}