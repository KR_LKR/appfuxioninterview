import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { CartController } from "./controllers/cart.controller";

import { Cart } from "./entities/cart.entity";
import { CartService } from "./services/cart.service";

@Module({
    imports:[TypeOrmModule.forFeature([Cart])],
    providers:[CartService],
    controllers:[CartController]
})
export class CartModule{}