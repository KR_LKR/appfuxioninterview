import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ProductsModule } from './products/products.module';
import { CartModule } from './cart/library.module';

import { TypeOrmModule } from '@nestjs/typeorm';

import { Cart } from 'src/cart/entities/cart.entity';
import { Product } from 'src/cart/entities/product.entity';


import { CartItem } from 'src/cart/entities/cart_item.entity';
import { Connection } from 'typeorm';


@Module({
  imports: [CartModule,ProductsModule,
    TypeOrmModule.forRoot({
    type: 'mysql',
    host: '34.124.222.99',
    port: 3306,
    username: 'root',
    password: 'Woshireng97',
    database: 'appfuxionInterview',
    entities:[Cart,CartItem,Product],
    // synchronize: true,
    logging: true
  }),
],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {private connection: Connection}
