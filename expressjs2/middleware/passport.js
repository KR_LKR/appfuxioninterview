const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const Consumer = require('../models').Consumer;
const Device = require('../models').Device;
const Admin = require('../models').Admin;

const ConsumerAuth = function(passport){
    var opts = {};
    
    opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
    opts.secretOrKey = CONFIG.jwt_encryption_consumer;
    passport.use(new JwtStrategy(opts, async function(jwt_payload, done){
       
        let err, consumer;

        if(jwt_payload.user_id!=null){

            [err, consumer] = await to(Consumer.findByPk(jwt_payload.user_id));

        }

        if(err){
            console.log("EROR")
            return done(err, false);

        } 

     

        if(consumer) 
        {

                return done(null, consumer);
           
        }   
        // Error msg handling multiple language 
        return done("Consumer Not Found", null);
        
    }));
}

module.exports.ConsumerAuth = ConsumerAuth;

const AdminAuth = function(passport){
    var opts = {};
    opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
    opts.secretOrKey = CONFIG.jwt_encryption_admin;
    
    passport.use(new JwtStrategy(opts, async function(jwt_payload, done){
        
        let err, admin;

        [err, admin] = await to(Admin.findById(jwt_payload.admin_id));


        if(err) return done(err, false);

        if(admin) 
        {
           //Add admin acc active checking
            if(admin.acc_active)
            {
                return done(null, admin);
            }else{
                return done('This account has been deactivated.', false);
            }
        }

        return done('Admin Not Found!', false);
        
    }));
}
module.exports.AdminAuth = AdminAuth;