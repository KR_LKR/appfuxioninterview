to = function(promise) {//global function that will help use handle promise rejections, this article talks about it http://blog.grossman.io/how-to-write-async-await-without-try-catch-blocks-in-javascript/
    return promise
    .then(data => {
        return [null, data];
    }).catch(err =>
        [pe(err)]
    );
}

pe = require('parse-error');//parses error so you can read error message and handle them accordingly

TE = function(err_message, log){ // TE stands for Throw Error
    if(log === true){
        console.error(err_message);
    }

    throw new Error(err_message);
}

ReE = function(res, err, code, data = null){ // Error Web Response
    
    if(typeof err == 'object' && typeof err.message != 'undefined'){
        err = err.message;
    }

    let send_data = {success:false, message: err};

    if(data && typeof data == 'object'){
        send_data = Object.assign(data, send_data);//merge the objects
    }

    if(typeof code !== 'undefined') res.statusCode = code;

    return res.json(send_data)
}

ReS = function(res, msg, data, code){ // Success Web Response
    let send_data = {success:true, message: msg};

    if(typeof data == 'object'){
        send_data = Object.assign(data, send_data);//merge the objects
    }

    if(typeof code !== 'undefined') res.statusCode = code;

    return res.json(send_data)
};

request = require('request');




randomStr = function(m,remove_unessery = false){

    var m = m || 9; s = '';
    let r = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890';
    
    if(remove_unessery)
    {    
        // r = 'ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz23456789';
        r = '0123456789';
    }

	for (var i=0; i < m; i++) { s += r.charAt(Math.floor(Math.random()*r.length)); }
    return s;
    
};

Keyfilter = function (body,filterkey){
    
    const filtered = Object.keys(body)
      .filter(key => filterkey.includes(key))
      .reduce((obj, key) => {
        obj[key] = body[key];
        return obj;
      }, {});

    return filtered;
}


