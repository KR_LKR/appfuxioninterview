var express = require('express');
var router = express.Router();  
const passport = require('passport');
const DeviceContoller = require('./../controller/DeviceController');
const { body,check, validationResult } = require('express-validator');
// const validate = require('./validation');


AuthMiddleware = function(req, res, next) {

    require('./../middleware/passport').ConsumerAuth(passport);
    passport.authenticate('jwt', function(err, user, info) {    
        if(!user)
        {   
            if(info)
                return ReE(res,info.message,401);
            return ReE(res,err,401);
        }
        req.consumer = user;
        return next();
    })(req, res, next);
  
};

router.post('/login', [
            body('username').isLength({ max: 15 }).notEmpty(),
            body('password').isLength({ max: 15 }).notEmpty(),
          ], DeviceContoller.login)
        
        
        
router.post('/signup', [
            body('username').isLength({ max: 15 }).notEmpty(),
            body('password').isLength({ max: 15 }).notEmpty(),
          ], DeviceContoller.Signup)


router.post('/productList',AuthMiddleware, DeviceContoller.ProductList)


router.post('/insertCart',AuthMiddleware,[
    body('product_id').notEmpty(),
  ],DeviceContoller.InsertCart)
router.post('/removeCart',AuthMiddleware, DeviceContoller.RemoveCart)
router.post('/myCart',AuthMiddleware, DeviceContoller.myCart)


module.exports = router;