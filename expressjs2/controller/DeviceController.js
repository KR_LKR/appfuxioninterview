const Device = require('../models').Device;
const MobileVerification = require('../models').MobileVerification;
const Consumer = require('../models').Consumer;
const Product = require('../models').Product;
const Cart = require('../models').Cart;
const CartItem = require('../models').CartItem;
const Media = require('../models').Media;
const Merchant = require('../models').merchant;
const SuggestBusiness = require('../models').SuggestBusiness;
const moment = require('moment');
require('../config/config');
const sequelize = require('../models').sequelize;
const { body ,validationResult} = require('express-validator')
const Sequelize = require('sequelize');
const op = Sequelize.Op;

const express = require('express')
const bodyParser = require('body-parser')
const { validate, ValidationError, Joi } = require('express-validation')
var bcrypt = require('bcryptjs');


exports.validate = (method) => {
    console.log("ABDDEFzz")
    switch (method) {
      case 'createUser': {
       return [ 
          body('userName', 'userName doesn\'t exists').exists(),
          body('email', 'Invalid email').exists().isEmail(),
          body('phone').optional().isInt(),
          body('status').optional().isIn(['enabled', 'disabled'])
         ]   
      }
    }
  }

const Signup = async function(req, res){
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() })
    }
    let consumer = await Consumer.findOne({where:{ username:req.body.username}});

    if(consumer){
        return ReE( res,'Username already exist',400 );
    }
    let consumer_data={}
    let passwd  = bcrypt.hashSync(req.body.password, 10);
    consumer_data.password=passwd
    consumer_data.username=req.body.username
    console.log(consumer_data)
    return Consumer.create(consumer_data).then( consumer => {
        return ReS( res, "Register Successful", {data:consumer.toWeb()},200);
    }).catch((err) => {
            return ReE(res,err,400);
    });
}
module.exports.Signup = Signup;

const login = async function(req, res){
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() })
    }
    let consumer = await Consumer.findOne({where:{ username:req.body.username}});

    if(!consumer){
        return ReE( res,'Usermaname Not Found',400 );
    }
    bcrypt.compare(req.body.password, consumer.password).then(async function(result) {
        if(result == true){
            let web_consumer = consumer.toWeb();
            return  ReS(res,'',{data:web_consumer,token:consumer.getJWT()});
        }
        else
        return ReE(res, 'Wrong password',400);

    }).catch((err) => {

        return ReE(res,err,400);
    });
}
module.exports.login = login;



const ProductList = async function(req, res){

    let limit = req.body.limit ?parseInt(req.body.limit) : 10
    let offset =req.body.offset?parseInt(req.body.offset) :0
    //  set a where condition for filtering  
    let where ={}

    let product = await Product.findAll(
        {limit:limit,
        offset:offset,
        where:where}
    );

    return  ReS(res,'',{data:product});
}
module.exports.ProductList = ProductList;


const InsertCart = async function(req, res){


    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() })
    }

    let User = req.consumer
    User=User.dataValues
    let product_id=req.body.product_id


    let product= await Product.findOne({where:{product_id:product_id,product_qty:{[op.gte]:1}}})

 
    if(!product)
        return ReE(res, "product not available", 400);

        //main function start here
        let cart = await Cart.findOne({where:{status:1,shop_id:product.shop_id,user_id:User.user_id}})
        if(cart)
        {
          let item = await CartItem.findOne({where:{cart_id:cart.cart_id,product_id:product_id,product_price:product.product_price}})

          if (!item){
             let item = await CartItem.create({cart_id:cart.cart_id,product_id:product_id,product_id:product_id,product_qty:1,product_price:product.product_price})
           }
           else{
             item.product_qty++
             item.save()
           }
        }
        else{
            cart={shop_id:product.shop_id,status:1,user_id:User.user_id}
            item={
                product_id:product_id,
                product_qty:1,
                product_price:product.product_price,
            }
            Cart.create(cart).then(data=>{
                item.cart_id=data.cart_id
                CartItem.create(item)

            })

        }
        return  ReS(res,'',{data:product});
}
module.exports.InsertCart = InsertCart;


const RemoveCart = async function(req, res){
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() })
    }

    let User = req.consumer
    User=User.dataValues
    let product_id=req.body.product_id
    let product= await Product.findOne({where:{product_id:product_id,product_qty:{[op.gte]:1}}})
    if(!product)
        return ReE(res, "product not available", 400);
    // let cart = await
    Cart.findOne({where:{status:1,shop_id:product.shop_id},
        include: [{
        as:"cartItem",
        model: CartItem,
        where:{product_id:product_id,product_price:product.product_price}
        //where: condition,
    }]}).then(data=>{
        console.log("data",data.cartItem[0].dataValues.product_qty)

        let newQty=data.cartItem[0].dataValues.product_qty-1
        console.log("NEWQTY",newQty)

        CartItem.update({product_qty:newQty },{ where: {cart_item_id:data.cartItem[0].dataValues.cart_item_id}}).then(update=>{
            return  ReS(res,'',{data:update});

        })

    })


}
module.exports.RemoveCart = RemoveCart;


const myCart = async function(req, res){

    let limit = req.body.limit ?parseInt(req.body.limit) : 10
    let offset =req.body.offset?parseInt(req.body.offset) :0
    //  set a where condition for filtering  
    let where ={}

    let User = req.consumer
    User=User.dataValues
    let product_id=req.body.product_id
    // let cart = await
    Cart.findAll({where:{status:1,user_id:User.user_id},limit:limit,offset:offset,
        include: [{
        as:"cartItem",
        model: CartItem,
        where:{product_qty:{[op.gte]:1}}
        //where: condition,
    }]}).then(data=>{

            return  ReS(res,'success',{data:data});


    })


}
module.exports.myCart = myCart;