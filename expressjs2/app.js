require('./config/config'); 
require('./global_functions');  

var createError = require('http-errors');
var express = require('express');
var logger = require('morgan');
const passport  = require('passport');
var bodyParser = require('body-parser')
var helmet = require('helmet');
const noCache = require('nocache')
var expressValidator = require('express-validator');



var homeRouter = require('./routes/home_r');

var moment = require('moment');
const { body } = require("express-validator");
var app = express();





app.use(helmet());
app.use(helmet.frameguard());
app.use(noCache())


app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
  });




app.use(logger(':method :url :status :response-time ms - :res[content-length] :remote-addr'));
app.use(bodyParser.json({defaultCharset: 'utf-8',limit: '50mb'}));
app.use(bodyParser.urlencoded({ extended: false,limit: '50mb'}));

//Passport
app.use(passport.initialize());

const models = require("./models");
models.sequelize.authenticate().then(() => {
    console.log('Connected to SQL database:', CONFIG.db_name);
})
.catch(err => {
    console.error('Unable to connect to SQL database:', CONFIG.db_name, err);
});

app.use(body(['*']).trim())


app.use(express.static('public'))

app.use('/', homeRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
  
    // render the error page
    res.status(err.status || 500).json({
      message: err.message,
      error: err
    });
});

function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

var port = normalizePort(process.env.PORT) || '3000';
app.listen(port, () => console.log(`Server listening on port ${port}`));
  

module.exports = app;
