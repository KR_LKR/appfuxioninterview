'use strict';
const jwt    = require('jsonwebtoken');
const moment = require('moment'); 
module.exports = (sequelize, DataTypes) => {

  var CartItem = sequelize.define('CartItem', {
    cart_item_id  : {type:DataTypes.INTEGER ,primaryKey: true,autoIncrement: true} ,
    cart_id: DataTypes.INTEGER,
    product_id: DataTypes.INTEGER,
    product_price: DataTypes.DECIMAL,
    product_qty: DataTypes.INTEGER,
    // discount_perc:DataTypes.INTEGER
  }, {
    freezeTableName: true,
    tableName: 'cart_item',
  });

  CartItem.associate = function(models) {

    CartItem.hasOne(models.Cart, {
      foreignKey: 'cart_id',
    });
  };

  return CartItem;

};