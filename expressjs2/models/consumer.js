'use strict';
const jwt    = require('jsonwebtoken');
const moment = require('moment'); 
module.exports = (sequelize, DataTypes) => {

  var Consumer = sequelize.define('Consumer', {
   
    username:DataTypes.STRING,
    user_id: {type:DataTypes.INTEGER ,primaryKey: true,autoIncrement: true} ,
    password: DataTypes.STRING,

  }, {
    freezeTableName: true,
    tableName: 'user',
 
  });


  Consumer.associate = function(models) {
   
 
  };

  Consumer.prototype.toWeb = function (pw) {
    let json = this.toJSON();
    delete json['password'];

    return json;
  };

  Consumer.prototype.getJWT = function () {
    
      
    let expiration_time = parseInt(CONFIG.jwt_expiration);
    console.log("IN MODEL")
    return "Bearer "+jwt.sign({user_id:this.user_id}, CONFIG.jwt_encryption_consumer, {expiresIn: expiration_time});
  };


  
  
  return Consumer;
};