'use strict';
const jwt    = require('jsonwebtoken');
const moment = require('moment'); 
module.exports = (sequelize, DataTypes) => {

  var Cart = sequelize.define('Cart', {
   
    cart_id  : {type:DataTypes.INTEGER ,primaryKey: true,autoIncrement: true} ,
    shop_id: DataTypes.INTEGER,
    status: DataTypes.INTEGER,
    user_id: DataTypes.INTEGER,

  }, {
    freezeTableName: true,
    timestamps: false,
    tableName: 'cart',
  });

  Cart.associate = function(models) {
    Cart.hasMany(models.CartItem, {
      foreignKey: 'cart_id',
      as:"cartItem"

    });
  };
  return Cart;
};