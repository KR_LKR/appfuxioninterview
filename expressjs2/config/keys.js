
require('dotenv').config();//instatiate environment variables

module.exports = {
    mongoURI: 'mongodb+srv://'+process.env.MONGO_USER+':'+process.env.MONGO_PASSWORD+'@'+process.env.MONGO_HOST+'/'+process.env.MONGO_NAME+'?retryWrites=true&w=majority',// connect to dev
}